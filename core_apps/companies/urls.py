from rest_framework import routers

from core_apps.companies.views import CompanyViewSet

companies_router = routers.DefaultRouter()
companies_router.register("companies", viewset=CompanyViewSet, basename="companies")

"""
companies_router.register("companies", viewset=CompanyViewSet, basename="companies")

:returns
{
    # companies: gotten from companies_router.register("companies", ......)
    "companies": "http://localhost:8000/companies/"
}
"""
