from django.contrib import admin

from core_apps.companies.models import Company


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ["name", "status", "last_update", "application_link", "notes"]
    list_filter = ["name", "status"]
    list_display_links = ["name", "status", "last_update", "application_link", "notes"]
