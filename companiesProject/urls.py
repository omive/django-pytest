from django.contrib import admin
from django.urls import path, include

from core_apps.companies.urls import companies_router

urlpatterns = [
    path("admin/", admin.site.urls),
    # companies urls
    path("", include(companies_router.urls)),
    # basic auth for REST framework's login and logout views. A
    path('auth/', include('rest_framework.urls')),
]
